ARK-3 eCFW for the PS Vita.

Features:

- Full compatibility with PSP home brews and games.

- ISO and CSO support through the Inferno ISO Driver as well as compatibility with the M33, ME and NP9660 drivers.

- Compatibility with PSX games under PSP exploits with partial sound through PEOPS.

- Partial compatibility with PSX exploits.

- Compatible with up to firmware 3.52

- Built in menu with advanced features like PMF playback, FTP, CFW settings and more. It is also compatible with other popular menus such as ONEmenu and 138Menu.



Credits:

- Coldbird for his work on the core of ARK.

- Liquid Snake (aka liquidzigong aka hrimfaxi aka virtuous flame) for his work on the core of ARK.

- neuron for his work on the core.

- qwikrazor87 for his kernel exploits and overall work on the core of the system.

- WTH/Yosh for his contributions to the project.

- Total_Noob for his help with PSX exploits.